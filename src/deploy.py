import os
import base64
from google.cloud import aiplatform

def upload_model_sample(
    project: str,
    location: str,
    display_name: str,
    serving_container_image_uri: str,
    artifact_uri: str,
    endpoint_display_name: str,
):

    # Google credentials should be assigned to a service account
    # Also note that resources should be configured to use the proper, restricted service account

    aiplatform.init(project=project, location=location)

    model = aiplatform.Model.upload(
        display_name=display_name,
        artifact_uri=artifact_uri,
        serving_container_image_uri=serving_container_image_uri,
    )

    model.wait()

    # Create an Endpoint
    endpoint = aiplatform.Endpoint.create(
        display_name=endpoint_display_name,
        project=project,
        location=location,
    )

    # Deploy the Model to the Endpoint
    endpoint.deploy(
        model=model,
        deployed_model_display_name=display_name,
        traffic_percentage=100,
        sync=True
    )

    print(model.display_name)
    print(model.resource_name)
    return model

upload_model_sample(
    project="", # Add your project name
    location="", # Add your project location
    display_name="", # Add display name
    serving_container_image_uri="us-docker.pkg.dev/vertex-ai/prediction/sklearn-cpu.1-2:latest",
    artifact_uri="", # Add directory/folder of model location
    endpoint_display_name="",  # Add endpoint display name  
)
